# TP4 #

L'objectif de ce TP est de créer une interface Web permettant d'accéder au service d'annuaire développé pendant le TP3. Cette interface sera créée avec le framework « Java Server Faces » présenté lors du dernier cours.

Il est demandé que l'accès au service se fasse à travers l'interface «Directory», mais pour l'instant l'implémentation effectivement utilisée importe peu (client REST ou accès direct à l'implémentation DatabaseDirectory).


Voir l'énoncé complet sur le [Moodle](http://foad2.unicaen.fr/moodle/course/view.php?id=24560).