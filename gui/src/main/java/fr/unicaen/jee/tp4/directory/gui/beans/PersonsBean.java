package fr.unicaen.jee.tp4.directory.gui.beans;

import java.util.*;
import javax.faces.bean.*;
import fr.unicaen.jee.tp3.directory.logic.*;

/**
 * @author Frédérik Bilhaut
 */
@ManagedBean(name="personsBean")
@ViewScoped
public class PersonsBean {
	
	private int currentPage = 0;
	private boolean moreElements = false;
	private final int itemsByPage = 5;
	
	@ManagedProperty(value="#{directoryBean}")
	private DirectoryBean directoryBean;	
	
	// Pour la propriété managée "directoryBean"
	public DirectoryBean getDirectoryBean() {
		return directoryBean;
	}

	// Pour la propriété managée "directoryBean"
	public void setDirectoryBean(DirectoryBean directoryBean)
	{
		this.directoryBean = directoryBean;
	}
	
	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int page) {
		if(page >= 0)
			this.currentPage = page;
	}		
	
	public void nextPage() {
		setCurrentPage(this.currentPage + 1);
	}
	
	public void previousPage() {
		setCurrentPage(this.currentPage - 1);
	}
	
	public boolean hasPreviousPage() {
		return this.currentPage > 0;
	}
	
	public boolean hasNextPage() {
		return this.moreElements;
	}
		
	public Collection<Person> getPersons() throws DirectoryException {
		Collection<Person> result = getDirectoryBean().getDirectory().getPersons(currentPage*itemsByPage, itemsByPage);
		this.moreElements = result.size() == this.itemsByPage;
		return result;
	}
	
}
