package fr.unicaen.jee.tp4.directory.gui.beans;

import javax.faces.bean.*;
import fr.unicaen.jee.tp3.directory.logic.*;

/**
 * @author Frédérik Bilhaut
 */
@ManagedBean(name="personBean")
@RequestScoped
public class PersonBean {
	
	@ManagedProperty(value="#{param.id}")
    private Long id = null;		
	
	@ManagedProperty(value="#{directoryBean}")
	private DirectoryBean directoryBean;	
	
	private Person value = null;
	
		
	public DirectoryBean getDirectoryBean() {
		return directoryBean;
	}
	
	public void setDirectoryBean(DirectoryBean directoryBean) {
		this.directoryBean = directoryBean;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;		
	}

	public synchronized Person getValue() throws DirectoryException {
		if(this.value == null)
			this.value = this.directoryBean.getDirectory().getPerson(this.id);
		return this.value;
	}
	
	
}
